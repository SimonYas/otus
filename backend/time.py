from flask import Flask
from time import gmtime, strftime


app = Flask(__name__)




@app.route('/time', methods=["GET"])
def time():
    time = strftime("%Y-%m-%d", gmtime())
    return time



if __name__ == "__main__":
    app.run(host = "0.0.0.0", port = 5002, threaded=True)
