import requests
from time import gmtime, strftime

def test_flask_service():
    try:
        r = requests.get('http://localhost:5000')
    except Exception as e:
        print(e)
    # get_page() returns a response object
    assert r.status_code == 200

def test_time_service():
    try:
        r = requests.get('http://localhost:5002/time')
    except Exception as e:
        print(e)
    assert r.status_code == 200
    assert r.text == strftime("%Y-%m-%d", gmtime())


def test_integration():
    try:
        r = requests.get('http://localhost:5000/')
    except Exception as e:
        print(e)
    time = str(strftime("%Y-%m-%d", gmtime()))
    page = str(r.text)
    assert page.find(time) != -1
